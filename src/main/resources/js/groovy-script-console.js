if (typeof $ === 'undefined') {
    $ = AJS.$;
}
$(function () {
    var editor;
    var markedLine=-1;
    var isHTML = RegExp.prototype.test.bind(/(<([^>]+)>)/i);
    function initCodeMirror() {
        editor = CodeMirror.fromTextArea(document.getElementById('groovy-script-console'), {
            matchBrackets: true,
            mode: "text/x-groovy",
            theme: "elegant",
            lineNumbers: true,
            extraKeys: {
                "F11": function (cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function (cm) {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            }
        });
    }

    function submitForm() {
        HoldOn.open({
            theme:"sk-bounce",
            message: AJS.I18n.getText('groovy-script-console.pleasewait')
        });
        resetUI();
        var dataObj = {};
        dataObj["script"] = btoa(editor.getValue());
        console.log(dataObj);
        $.ajax({
            type: 'POST',
            timeout: 0,
            contentType: "application/json; charset=UTF-8",
            dataType: 'json',
            data: JSON.stringify(dataObj),
            url: AJS.contextPath()+'/rest/groovy-script-console/latest/script/execute'
        })
        .done(function(result){
            HoldOn.close();
            $('#groovy-script-console-output').show();
            console.log(result);
            if (result.totalTime){
                $('#groovy-script-console-execution-time').append(AJS.format(AJS.I18n.getText('groovy-script-console.admin.run.executiontime'), result.totalTime));
            }
            var elementToInsert="";
            if (result.resultAsString && result.resultAsString.length > 0){
                elementToInsert = result.resultAsString;
                if (!isHTML(elementToInsert)){
                    elementToInsert="<pre><code>"+elementToInsert+"</code></pre>";
                }
                $('#groovy-script-console-tab-script-result').empty().append(elementToInsert);
                AJS.tabs.change($('a[href="#groovy-script-console-tab-script-result"]'));
            }
            elementToInsert="";
            if (result.log && result.log.length > 0){
                elementToInsert = result.log;
                elementToInsert="<pre><code>"+elementToInsert+"</code></pre>";
                $('#groovy-script-console-tab-log-messages').empty().append(elementToInsert);
            }
            elementToInsert="";
            if (result.exceptionAsString && result.exceptionAsString.length > 0){
                elementToInsert = result.exceptionAsString;
                elementToInsert="<pre><code>"+elementToInsert+"</code></pre>";
                $('#groovy-script-console-tab-exception').empty().append(elementToInsert);
                AJS.tabs.change($('a[href="#groovy-script-console-tab-exception"]'));
            }
            if (result.errorLineNumber && result.errorLineNumber>0){
                markedLine=result.errorLineNumber-1;
                editor.addLineClass(markedLine, 'background', 'groovy-script-console-line-error');
            }
        })
        .fail(function(xhr, status, error) {
            HoldOn.close();
            showErrorMessage(xhr.statusText,status);
            console.log(xhr.statusText);
            console.log(status);
            console.log(error);
        });
    }

    function resetUI(){
        //hide all messages
        $('#groovy-script-console-message').find('span.icon-close').click();
        $('#groovy-script-console-message').hide();
        $('#groovy-script-console-output').hide();
        $('#groovy-script-console-execution-time').empty();
        $('#groovy-script-console-tab-script-result').empty();
        $('#groovy-script-console-tab-log-messages').empty();
        $('#groovy-script-console-tab-exception').empty();
        if (markedLine && markedLine>=0){
            editor.removeLineClass(markedLine);
        }
    }

    function showErrorMessage(title,message){
        AJS.messages.error("#groovy-script-console-message", {
            title: title,
            body: '<p>'+message+'</p>'
        });
        $('#groovy-script-console-message').show();
    }

    function initShortcuts() {
        //$(document).on('keydown', null, 'ctrl+x', submitForm);
    }

    function initSubmitListener() {
        $('#script-console-execute').on('click', submitForm);
    }

    initCodeMirror();
    initShortcuts(submitForm);
    initSubmitListener(submitForm);
});