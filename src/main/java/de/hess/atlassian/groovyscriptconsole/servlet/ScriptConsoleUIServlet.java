package de.hess.atlassian.groovyscriptconsole.servlet;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.net.URI;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.atlassian.plugin.spring.scanner.ProductFilter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import javax.inject.Inject;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import de.hess.atlassian.groovyscriptconsole.util.ProductFilterUtil;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Scanned
public class ScriptConsoleUIServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(ScriptConsoleUIServlet.class);

    @ComponentImport
    private final UserManager userManager;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;
    @ComponentImport
    private final TemplateRenderer templateRenderer;
    @ComponentImport
    private final I18nResolver i18n;
    @ComponentImport
    private final ApplicationProperties applicationProperties;

    @Inject
    public ScriptConsoleUIServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer templateRenderer,
                                  I18nResolver i18n, ApplicationProperties applicationProperties) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
        this.i18n = i18n;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username)) {
            redirectToLogin(request, response);
            return;
        }
        Map<String, Object> context = new HashMap<String, Object>();
        context.put("baseUrl", applicationProperties.getBaseUrl(UrlMode.CANONICAL));
        context.put("product", getAtlassianProduct());
        String link = i18n.getText("groovy-script-console.admin.description.links."+getAtlassianProduct());
        if (!StringUtils.startsWith(link,"http")){
            link = "https://developer.atlassian.com/";
        }
        context.put("link2api", link);
        response.setContentType("text/html;charset=utf-8");
        templateRenderer.render("templates/groovy-script-console/admin/groovy-script-console.vm", context, response.getWriter());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse response)
        throws IOException {
        doGet(req, response);
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private ProductFilter getAtlassianProduct() {
        BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
        ProductFilter productFilter = ProductFilter.ALL;
        try {
            productFilter = ProductFilterUtil.getFilterForCurrentProduct(bundleContext);
        } catch (Throwable ex){
            log.error("Error while detecting Atlassian product using All.",ex);
        }
        return productFilter;
    }
}