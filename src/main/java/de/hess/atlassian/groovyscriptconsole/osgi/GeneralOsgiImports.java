package de.hess.atlassian.groovyscriptconsole.osgi;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Named;

@SuppressWarnings("UnusedDeclaration")
@Named
public class GeneralOsgiImports {
    @ComponentImport
    com.atlassian.sal.api.user.UserManager userManager;
}
