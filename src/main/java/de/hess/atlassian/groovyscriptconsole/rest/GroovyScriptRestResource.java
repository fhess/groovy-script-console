package de.hess.atlassian.groovyscriptconsole.rest;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.multipart.FilePart;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.sun.jersey.core.util.Base64;
import de.hess.atlassian.groovyscriptconsole.component.ScriptingManager;
import com.atlassian.plugins.rest.common.multipart.MultipartFormParam;
import de.hess.atlassian.groovyscriptconsole.model.ErrorMessage;
import de.hess.atlassian.groovyscriptconsole.model.ScriptExecutionResult;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

@Path("/script")
@Named
public class GroovyScriptRestResource {
    private static final Logger log = LoggerFactory.getLogger(GroovyScriptRestResource.class);

    private final UserManager userManager;
    private final ScriptingManager scriptingManager;

    @Inject
    public GroovyScriptRestResource(UserManager userManager, ScriptingManager scriptingManager) {
        this.userManager = userManager;
        this.scriptingManager = scriptingManager;
    }

    @POST
    @Path("/executeFile")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response executeScriptFile(@Context HttpServletRequest request,
                                      @MultipartFormParam("file") FilePart filePart) {
        UserKey user = userManager.getRemoteUserKey(request);
        if (user == null || !userManager.isSystemAdmin(user)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        if (log.isDebugEnabled()) {
            String fileName = filePart.getName();
            log.debug("Processing file with name {}", fileName);
        }
        try {
            InputStream is = filePart.getInputStream();
            String text = IOUtils.toString(is, StandardCharsets.UTF_8.name());
            ScriptExecutionResult result = scriptingManager.executeScript(text);
            return Response.ok(result).cacheControl(never()).build();
        } catch (IOException ex) {
            log.error("Error while reading groovy file to process.", ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new ErrorMessage(ex,"Error while reading groovy file to process."))
                .cacheControl(never())
                .type(MediaType.APPLICATION_JSON).
                    build();
        }
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/execute")
    public Response execute(@Context HttpServletRequest request, String jsonMessage) {
        UserKey user = userManager.getRemoteUserKey(request);
        if (user == null || !userManager.isSystemAdmin(user)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
        if (log.isDebugEnabled()) {
            log.debug("Processing message {}", jsonMessage);
        }
        try {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<HashMap<String, String>> typeRef
                = new TypeReference<HashMap<String, String>>() {
            };
            HashMap<String, String> map = mapper.readValue(jsonMessage, typeRef);
            String script = map.get("script");
            if (Base64.isBase64(script)) {
                script = Base64.base64Decode(script);
            }
            ScriptExecutionResult result = scriptingManager.executeScript(script);
            return Response.ok(result).cacheControl(never()).build();
        } catch (IOException ex) {
            log.error("Error while parsing json string.", ex);
            return Response.status(500)
                .cacheControl(never())
                .entity(new ErrorMessage(ex,"Error while parsing json string."))
                .type(MediaType.APPLICATION_JSON).
                    build();
        }
    }


    private static CacheControl never() {
        CacheControl cacheNever = new CacheControl();
        cacheNever.setNoStore(true);
        cacheNever.setNoCache(true);
        return cacheNever;
    }
}
