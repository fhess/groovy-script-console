package de.hess.atlassian.groovyscriptconsole.util;
/**
 * no idea why com.atlassian.plugin.spring.scanner.util.ProductFilterUtil isn't visible ... including it here
 */

import com.atlassian.plugin.spring.scanner.ProductFilter;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Utility to figure out what the currently running product is and give us it's name/filter in various ways
 */
public class ProductFilterUtil {
    private static ProductFilterUtil INSTANCE;

    @VisibleForTesting
    static final String CLASS_ON_BAMBOO_CLASSPATH = "com.atlassian.bamboo.build.BuildExecutionManager";
    @VisibleForTesting
    static final String CLASS_ON_BITBUCKET_CLASSPATH = "com.atlassian.bitbucket.repository.RepositoryService";
    @VisibleForTesting
    static final String CLASS_ON_CONFLUENCE_CLASSPATH = "com.atlassian.confluence.core.ContentEntityManager";
    @VisibleForTesting
    static final String CLASS_ON_FECRU_CLASSPATH = "com.atlassian.fisheye.spi.services.RepositoryService";
    @VisibleForTesting
    static final String CLASS_ON_JIRA_CLASSPATH = "com.atlassian.jira.bc.issue.IssueService";
    @VisibleForTesting
    static final String CLASS_ON_REFAPP_CLASSPATH = "com.atlassian.refapp.api.ConnectionProvider";
    @VisibleForTesting
    static final String CLASS_ON_STASH_CLASSPATH = "com.atlassian.stash.repository.RepositoryService";

    private static final Logger log = LoggerFactory.getLogger(ProductFilterUtil.class);

    private AtomicReference<ProductFilter> filterForProduct = new AtomicReference<ProductFilter>();

    private ProductFilterUtil() {
    }

    /**
     * @param bundleContext - the bundle context
     * @return the ProductFilter instance that represents the currently running product.
     */
    public static ProductFilter getFilterForCurrentProduct(BundleContext bundleContext) {
        return getInstance().getFilterForProduct(bundleContext);
    }

    public static String getClassForCurrentProduct(ProductFilter productFilter) {
        for (Map.Entry<String, ProductFilter> entry : ProductFilterUtil.PRODUCTS_TO_HOSTCLASSES.entrySet()) {
            if (entry.getValue().equals(productFilter)) {
                return entry.getKey();
            }
        }
        return CLASS_ON_JIRA_CLASSPATH;
    }

    public ProductFilter getFilterForProduct(final BundleContext bundleContext) {
        // lazy building of known product.
        ProductFilter productFilter = filterForProduct.get();
        if (productFilter == null) {
            filterForProduct.compareAndSet(productFilter, detectProduct(bundleContext));
            productFilter = filterForProduct.get();
        }
        return productFilter;
    }

    /**
     * We try to detect a OSGi service that is uniquely offered by the host.
     * <p/>
     * You should extend out this method as we add support for other products
     */
    private final static Map<String, ProductFilter> PRODUCTS_TO_HOSTCLASSES = ImmutableMap.<String, ProductFilter>builder()
        .put(CLASS_ON_BAMBOO_CLASSPATH, ProductFilter.BAMBOO)
        .put(CLASS_ON_BITBUCKET_CLASSPATH, ProductFilter.BITBUCKET)
        .put(CLASS_ON_CONFLUENCE_CLASSPATH, ProductFilter.CONFLUENCE)
        .put(CLASS_ON_FECRU_CLASSPATH, ProductFilter.FECRU)
        .put(CLASS_ON_JIRA_CLASSPATH, ProductFilter.JIRA)
        .put(CLASS_ON_REFAPP_CLASSPATH, ProductFilter.REFAPP)
        .put(CLASS_ON_STASH_CLASSPATH, ProductFilter.STASH)

        .build();

    private ProductFilter detectProduct(final BundleContext bundleContext) {
        if (bundleContext == null) {
            log.warn("Couldn't detect product due to null bundleContext: will use ProductFilter.ALL");
            return ProductFilter.ALL;
        }

        for (Map.Entry<String, ProductFilter> entry : PRODUCTS_TO_HOSTCLASSES.entrySet()) {
            if (detectService(bundleContext, entry.getKey())) {
                log.debug("Detected product: " + entry.getValue().name());
                return entry.getValue();
            }
        }

        log.warn("Couldn't detect product, no known services found: will use ProductFilter.ALL");
        return ProductFilter.ALL;
    }

    private boolean detectService(final BundleContext bundleContext, final String serviceClassName) {
        // We don't actually need to get the service, we just check we can get a reference
        return null != bundleContext.getServiceReference(serviceClassName);
    }

    private static ProductFilterUtil getInstance() {
        if (null == INSTANCE) {
            INSTANCE = new ProductFilterUtil();
        }

        return INSTANCE;
    }

}
