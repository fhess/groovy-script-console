package de.hess.atlassian.groovyscriptconsole.component;

import de.hess.atlassian.groovyscriptconsole.model.ScriptExecutionResult;
import groovy.lang.Binding;

import java.io.File;
import java.io.IOException;

/**
 * Encapsulate script execution and syntax validation.
 */
@SuppressWarnings("unused")
public interface ScriptingManager {
    /**
     * Execute provided script using default script bindings.
     * @param src groovy script as string
     * @return result from script
     */
    ScriptExecutionResult executeScript(String src);

    /**
     * Execute provided script using provided script bindings.
     * @param src file to parse and execute
     * @param context binding parameter for script, logger will be added if not provided
     * @return result from script
     */
    ScriptExecutionResult executeScript(String src, Binding context);

    /**
     * Execute provided groovy file using default script bindings.
     * @param srcFile file to parse and execute
     * @return result from script
     * @throws IOException failed reading input file
     */
    ScriptExecutionResult executeScript(File srcFile) throws IOException;

    /**
     * Execute provided groovy file using provided script bindings.
     * @param srcFile file to parse and execute
     * @param context binding parameter for script, logger will be added if not provided
     * @return result from script
     * @throws IOException failed reading input file
     */
    ScriptExecutionResult executeScript(File srcFile, Binding context) throws IOException;

    /**
     * Validate script syntax.
     * @param groovyScript groovy script as string
     * @return validation result
     */
    ScriptExecutionResult validateSyntax(String groovyScript);
}
