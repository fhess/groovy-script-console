package de.hess.atlassian.groovyscriptconsole.component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.component.ComponentLocator;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import de.hess.atlassian.groovyscriptconsole.model.ScriptExecutionResult;
import de.hess.atlassian.groovyscriptconsole.util.JoinClassLoader;
import de.hess.atlassian.groovyscriptconsole.util.ProductFilterUtil;
import groovy.lang.GroovyClassLoader;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ExportAsService(ScriptingManagerImpl.class)
@SuppressWarnings("unused")
@Named
public class ScriptingManagerImpl implements ScriptingManager {
    private static final Logger log = Logger.getLogger(ScriptingManagerImpl.class);
    private final Cache<String, Class> scriptCache;

    public ScriptingManagerImpl(){
        this.scriptCache = CacheBuilder.newBuilder().maximumSize(1000L).expireAfterAccess(1L, TimeUnit.HOURS).build();
    }

    public ScriptExecutionResult executeScript(String src)
    {
        return executeScript(src, getDefaultBinding());
    }

    @SuppressWarnings("unchecked")
    public ScriptExecutionResult executeScript(String src, Binding context)
    {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        StringWriter loggerStringWriter = new StringWriter();
        Object result = null;
        Throwable exception = null;
        GroovyClassLoader groovyClassLoader = getGroovyClassloader(null);
        final ClassLoader org = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(groovyClassLoader);
        try {
            //use reflection here to isolate classloader -> otherwise class a is not class a during script execution anymore !
            String hash = DigestUtils.md5Hex(src);
            Class scriptClass = scriptCache.get(hash, () -> groovyClassLoader.parseClass(src));
            Object scriptInstance = scriptClass.newInstance();
            //binding can only be set for scripts
            try {
                Class bindingClass = groovyClassLoader.loadClass("groovy.lang.Binding");
                Method bindingSetter = scriptClass.getMethod("setBinding",bindingClass);
                Constructor<?> bindingConstructor = bindingClass.getConstructor(Map.class);
                Map<String,Object> bindings = new HashMap<>();
                bindings.put("log",getScriptLogger(loggerStringWriter));
                Object binding = bindingConstructor.newInstance(new HashMap<>(bindings));
                bindingSetter.invoke(scriptInstance, binding);
            } catch (Exception ex){
                log.debug("Binding can't be set probably class syntax was used -> Ignoring but logger is not set.",ex);
            }
            result = scriptClass.getDeclaredMethod("run",new Class[] {}).invoke(scriptInstance);
        } catch (Exception ex) {
            exception = ex;
        }
        finally {
            stopWatch.stop();
            Thread.currentThread().setContextClassLoader(org);
        }
        if (exception!=null){
            return new ScriptExecutionResult(exception, getStackTraceAsString(exception), getExceptionLineNumber(exception));
        }
        return new ScriptExecutionResult(result,getResultAsString(result),stopWatch.getTime(),loggerStringWriter.toString());
    }

    public ScriptExecutionResult executeScript(File srcFile, Binding context) throws IOException {
        String src = FileUtils.readFileToString(srcFile);
        return executeScript(src,context);
    }

    public ScriptExecutionResult executeScript(File srcFile) throws IOException {
        String src = FileUtils.readFileToString(srcFile);
        return executeScript(src,getDefaultBinding());
    }

    public ScriptExecutionResult validateSyntax(String groovyScript) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Throwable exception = null;
        try {
            GroovyShell shell = new GroovyShell(getGroovyClassloader(null));
            shell.parse(groovyScript);
        } catch (Exception ex) {
            exception = ex;
        } finally {
            stopWatch.stop();
        }
        if (exception!=null){
            return new ScriptExecutionResult(exception, getStackTraceAsString(exception), getExceptionLineNumber(exception));
        }
        return new ScriptExecutionResult(null,null,null);
    }

    private Object createBindings(ClassLoader loader, Map<String, Object> map) {
        try {
            Constructor<?> bindingConstructor = loader.loadClass("groovy.lang.Binding").getConstructor(Map.class);
            return bindingConstructor.newInstance(new HashMap<>(map));
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException
            | IllegalAccessException e) {
            throw new AssertionError(e);
        }
    }

    private Binding getDefaultBinding() {
        HashMap<String, Object> defaultVariables = new HashMap<>();
        return new Binding(defaultVariables);
    }

    @SuppressWarnings("unchecked")
    private Binding addScriptLoggerToBinding(Binding binding, final StringWriter loggerStringWriter) {
        Objects.requireNonNull(binding);
        Map variables = binding.getVariables();
        variables.put("log",getScriptLogger(loggerStringWriter));
        return new Binding(variables);
    }

    private Logger getScriptLogger(final StringWriter loggerStringWriter){
        Logger scripLogger = Logger.getLogger(ScriptingManagerImpl.class + "-"+Thread.currentThread().getId()+"-script");
        scripLogger.removeAllAppenders();
        scripLogger.setLevel(Level.ALL);
        ConsoleAppender ca = new ConsoleAppender();
        ca.setWriter(loggerStringWriter);
        ca.setName("scripLogger");
        ca.setLayout(new PatternLayout("%-5p [%t]: %m%n"));
        scripLogger.addAppender(ca);
        return scripLogger;
    }

    private String getResultAsString(Object result) {
        String resultAsString;
        if (result instanceof String) {
            resultAsString = (String) result;
        }else{
            resultAsString = ReflectionToStringBuilder.toString(result);
        }
        return resultAsString;
        //return InvokerHelper.inspect(result);
    }

    private String getStackTraceAsString(Throwable ex) {
        Objects.requireNonNull(ex);
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    private Integer getExceptionLineNumber(Throwable ex){
        Objects.requireNonNull(ex);
        //parse: at Script1.run(Script1.groovy:2)
        Integer scriptLine = null;
        String patternString = "at Script\\d+\\.run\\(Script\\d+\\.groovy:(\\d+)\\)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(getStackTraceAsString(ex));
        while(matcher.find()) {
            scriptLine = Integer.parseInt(matcher.group(1));
        }
        return scriptLine;
    }

    private GroovyClassLoader getGroovyClassloader(ClassLoader classLoader) {
        BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
        CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
        compilerConfiguration.setSourceEncoding("UTF-8");
        JoinClassLoader joinClassLoader;
        if (classLoader==null){
            joinClassLoader = new JoinClassLoader(
                bundleContext.getServiceReference(ProductFilterUtil.getClassForCurrentProduct(ProductFilterUtil.getFilterForCurrentProduct(bundleContext))).getClass().getClassLoader(),
                this.getClass().getClassLoader());
        } else {
            joinClassLoader = new JoinClassLoader(
                classLoader,
                bundleContext.getServiceReference(ProductFilterUtil.getClassForCurrentProduct(ProductFilterUtil.getFilterForCurrentProduct(bundleContext))).getClass().getClassLoader(),
                this.getClass().getClassLoader()
            );
        }
        GroovyClassLoader groovyLoader = new GroovyClassLoader(joinClassLoader,compilerConfiguration);
        groovyLoader.setShouldRecompile(true);
        return groovyLoader;
    }
}
