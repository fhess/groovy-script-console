package de.hess.atlassian.groovyscriptconsole.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@SuppressWarnings({"unused", "WeakerAccess"})
@XmlRootElement
public class ScriptExecutionResult {
    @XmlTransient
    private Object result;
    @XmlElement
    private String resultAsString;
    @XmlElement
    private Long totalTime;
    @XmlElement
    private String log;
    @XmlTransient
    private Throwable exception;
    @XmlElement
    private String exceptionAsString;
    @XmlElement
    private Integer errorLineNumber;

    public ScriptExecutionResult() {

    }

    public ScriptExecutionResult(final Object result, final String resultAsString, final Long totalTime,
                                 final String log) {
        this.result = result;
        this.resultAsString = resultAsString;
        this.totalTime = totalTime;
        this.log = log;
    }

    public ScriptExecutionResult(final Throwable exception, final String exceptionAsString, final Integer errorLineNumber) {
        this.exception = exception;
        this.exceptionAsString = exceptionAsString;
        this.errorLineNumber = errorLineNumber;
    }

    public Object getResult() {
        return result;
    }

    public Throwable getException() {
        return exception;
    }

    public Long getTotalTime() {
        return totalTime;
    }

    public String getLog() {
        return log;
    }

    public String getResultAsString() {
        return resultAsString;
    }

    public String getExceptionAsString() {
        return exceptionAsString;
    }

    public Integer getErrorLineNumber() {
        return errorLineNumber;
    }
}
