package de.hess.atlassian.groovyscriptconsole.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.beanutils.BeanUtils;


@SuppressWarnings({"unused", "WeakerAccess"})
@XmlRootElement
public class ErrorMessage {

    @XmlElement(name = "status")
    private int status;

    @XmlElement(name = "code")
    private int code;

    @XmlElement(name = "message")
    private String message;

    @XmlElement(name = "developerMessage")
    private String developerMessage;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public void setDeveloperMessage(String developerMessage) {
        this.developerMessage = developerMessage;
    }

    public ErrorMessage(Exception ex){
        try {
            BeanUtils.copyProperties(this, ex);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ErrorMessage(Exception ex, String developerMessage){
        try {
            BeanUtils.copyProperties(this, ex);
            setDeveloperMessage(developerMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ErrorMessage() {}
}
