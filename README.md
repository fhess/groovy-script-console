# Groovy Console for Atlassian products

## Overview
This add-on provides a groovy script console for Atlassian products (Jira, Confluence,..). 
Use cases for the console may be troubleshooting in production or support during plugin development. 

## Using the console
You can use the console diretly from within the Atlassian product or using the provided REST API. 
Demo scripts can be found in the scripts folder.

*supported syntax*
- script syntax
- class syntax (whithout provided logger)
- wrapped syntax

see scripts/syntax

*binding*

| Variable | Class                   | Purpose                              |
|----------|-------------------------|--------------------------------------|
| log      | org.apache.log4j.Logger | integrated logger see scripts/logger |


### Using from Atlassian product
Edit script diretly in console and press the execute button. UI (e.g. link to API) is adapted per product.

![Groovy Script Console](docs/images/groovy-script-console.png)

### Using from console
You can use curl to upload your file. See scripts/hello_world/hello_world.sh for an example bash script.
```
curl -D- -u admin:admin -X POST -H "X-Atlassian-Token: no-check" -F "file=@hello_world.groovy" http://localhost:2990/jira/rest/groovy-script-console/latest/script/executeFile
```

### Using from IntelliJ
Define external tool to profit from code completion and syntax highlighting in IntelliJ and execute groovy script in 
application.

*Steps:*
- create a plugin project for your product e.g. for Jira
```
atlas-create-jira-plugin
```
- define external tool see https://www.jetbrains.com/help/idea/external-tools.html -> "run in Groovy console"
```
Program: curl
Parameters:  -D- -u admin:admin -X POST -H "X-Atlassian-Token: no-check" -F "file=@$FileName$" http://localhost:5990/refapp/rest/groovy-script-console/latest/script/executeFile
Working directory: $FileDir$
```
- create a groovy file
- right click on editor tab -> External Tools -> "run in Groovy console"

See [IntelliJ Integration](docs/intellij.md) for more advanced integration.

## Add-on development and Pull requests
Add-on is developed under Apache license and pull requests are welcome.

### Build add-on
To build the project, make sure you have the Atlassian SDK installed
and then `atlas-packge`. To run 
- the RefApp `atlas-run` and open url `http://localhost:5990/refapp`
- Jira `atlas-mvn  amps:debug -Dproduct=jira` and open url `http://fhess-laptop:2990/jira`

### License
http://www.apache.org/licenses/LICENSE-2.0.html