# IntelliJ Integration
## Helper Script
Create bash script ~/bin/executeGroovyScriptInConsole.sh
```
#!/usr/bin/env bash
set -x
FILE=${1}
USER=${2:-admin_avono}
PWD=${3:-admin}
HOST=${4:-http://localhost:8080}
curl -u $USER:$PWD -X POST -H "X-Atlassian-Token: no-check" -F "file=@$FILE" $HOST/rest/groovy-script-console/latest/script/executeFile | xmllint --format -

```
Configure external tool

https://www.jetbrains.com/help/idea/external-tools.html

```
Program: /home/fhess/bin/executeGroovyScriptInConsole.sh
Arguments: $FileName$
Working directory: $FileDir$
```
Optional: Define Shortcut for external tool
https://www.jetbrains.com/help/idea/configuring-keyboard-shortcuts.html

```
Shortcut: STRG+UMSCHALT+X
```

Execute groovy script in console by pressing STRG+UMSCHALT+X while editing groovy script file
 
