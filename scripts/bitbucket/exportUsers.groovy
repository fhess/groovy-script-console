/*
    script to export all users/groups
 */
import com.atlassian.bitbucket.user.DetailedGroup
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.bitbucket.user.UserService
import com.atlassian.bitbucket.util.Page
import com.atlassian.bitbucket.user.UserAdminService
import com.atlassian.bitbucket.user.ApplicationUser
import com.atlassian.bitbucket.util.PageRequest
import com.atlassian.bitbucket.util.PageRequestImpl


def userService = ComponentLocator.getComponent(UserService)
def userAdminService = ComponentLocator.getComponent(UserAdminService)

StringBuffer sb = new StringBuffer()

Page<DetailedGroup> allGroups = userAdminService.findGroups(new PageRequestImpl(0,PageRequest.MAX_PAGE_LIMIT))
allGroups.values.each {
    DetailedGroup detailedGroup->
        sb << detailedGroup.name
        Page<ApplicationUser> usersByGroup = userService.findUsersByGroup(detailedGroup.name,new PageRequestImpl(0,PageRequest.MAX_PAGE_LIMIT))
        usersByGroup.values.each {
            ApplicationUser aUser ->
                sb << detailedGroup.name<<";"<<aUser.name << "<br/>"
        }

}
return sb.toString()