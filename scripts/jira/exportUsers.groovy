/*
script to export all users/groups
 */
import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser

GroupManager groupManager = ComponentAccessor.getGroupManager();
StringBuffer sb = new StringBuffer()
Collection<Group> allGroups = groupManager.getAllGroups()
allGroups.each {Group aGroup ->
    groupManager.getUsersInGroup(aGroup.getName()).each {
        ApplicationUser applicationUser ->
            if (applicationUser.isActive()){
                sb << aGroup.getName()<<";" << applicationUser.name <<"<br/>"
            }
    }
}
return sb.toString()