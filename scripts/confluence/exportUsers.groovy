/*
    script to export all users/groups
 */
import com.atlassian.confluence.user.ConfluenceUser
import com.atlassian.spring.container.ContainerManager
import com.atlassian.confluence.user.UserAccessor
import com.atlassian.user.Group
import com.atlassian.confluence.user.ConfluenceUser

StringBuffer sb = new StringBuffer()
UserAccessor userAccessor = ContainerManager.getComponent("userAccessor") as UserAccessor
List<Group> groupList = userAccessor.getGroupsAsList();
for (Group aGroup:groupList){
    Iterable<ConfluenceUser> users = userAccessor.getMembers(aGroup)
    Iterator<ConfluenceUser> userIterator = users.iterator()
    while (userIterator.hasNext()){
        sb << aGroup.getName() << ";" << userIterator.next().getName()<< "<br/>"
    }
}
return sb.toString()