import org.apache.log4j.Level
import org.apache.log4j.Logger

Logger logger = log as Logger

logger.setLevel(Level.ERROR)
logger.error("Error message")
logger.debug("Debug message will not show.")
return "logger demo."