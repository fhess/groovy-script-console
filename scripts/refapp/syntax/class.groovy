import com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory
import groovy.sql.Sql
import org.apache.commons.lang.StringEscapeUtils

public class SqlDemo {
    public run(){
        def sql = null
        try {
            sql = Sql.newInstance(new DefaultOfBizConnectionFactory().getConnection())
            def rows = sql.rows('select * from "cwd_user"')
            def result = rows.join('\n')
            return "<pre>"+StringEscapeUtils.escapeHtml(result)+"</pre>"
        } finally {
            sql?.close()
        }
    }
}

